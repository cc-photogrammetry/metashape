#!/usr/bin/env python
# vim: set softtabstop=2 ts=2 sw=2 expandtab:
import os, Metashape, sys, yaml 

""" 
 Create the metashape document that we will be proccessing
 Data Dir: The directory comes from the environment variable METASHAPE_DATA_PATH
 Project Name: Data Dir + SLURM JOB ID + -project.psz
"""
projectName = os.environ['METASHAPE_PROJECT_NAME']
path_photos = os.environ['METASHAPE_DATA_PATH']
orbitName = os.path.basename(os.path.normpath(path_photos))
projectPath =  path_photos + '/' + projectName + '-' + orbitName + '.psz'
""" Set the Metashape settings """
Metashape.app.Settings.log_enable = True
""" Enable GPU by unmasking it """
Metashape.app.gpu_mask = 15
Metashape.app.Settings.log_path = path_photos+'/' + os.environ['SLURM_JOB_ID'] + '-metashape.log'
""" We are always running this against a subdir """
try:
  jobSettingsFile = open(path_photos + '/../jobsettings.yaml')
  jobSettings = yaml.load(jobSettingsFile)
except:
  print("Error:: Unable to open job settings file %s" % (path_photos + '/../jobsettings.yaml'))
  sys.exit(128)
doc = Metashape.app.document
doc.open(projectPath)
chunk = doc.chunk
chunk.refineModel(downscale=1, iterations=20, smoothness=0.5)
doc.save(path = projectPath)
print("Saved the project, exiting")
sys.exit(0)
