#!/usr/bin/env python
# vim: set softtabstop=2 ts=2 sw=2 expandtab:
import os, Metashape, sys, yaml 

""" 
 Create the metashape document that we will be proccessing
 Data Dir: The directory comes from the environment variable METASHAPE_DATA_PATH
 Project Name: Data Dir + SLURM JOB ID + -project.psz
"""
path_photos = os.environ['METASHAPE_DATA_PATH']
projectName = os.environ['METASHAPE_PROJECT_NAME']
orbitName = os.path.basename(os.path.normpath(path_photos))
""" Set the Metashape settings """
Metashape.app.Settings.log_enable = True
Metashape.app.Settings.log_path = path_photos+'/' + os.environ['SLURM_JOB_ID'] + '-metashape.log'
doc=Metashape.Document()
if Metashape.Application.activated:
  print ("Metashape Activation Successfull")
else:
  print ("Metashape Activation Failure, re-init failed")
  sys.exit(128)
sys.exit(0)
