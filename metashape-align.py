#!/usr/bin/env python
# vim: set softtabstop=2 ts=2 sw=2 expandtab:
import os, Metashape, sys, yaml 

""" 
 Create the metashape document that we will be proccessing
 Data Dir: The directory comes from the environment variable METASHAPE_DATA_PATH
 Project Name: Data Dir + SLURM JOB ID + -project.psz
"""
path_photos = os.environ['METASHAPE_DATA_PATH']
projectName = os.environ['METASHAPE_PROJECT_NAME']
orbitName = os.path.basename(os.path.normpath(path_photos))
""" Set the Metashape settings """
Metashape.app.Settings.log_enable = True
Metashape.app.Settings.log_path = path_photos+'/' + os.environ['SLURM_JOB_ID'] + '-metashape.log'
doc=Metashape.Document()
if Metashape.app.activated == True:
  print ("Metashape Activation succeded, running job")
else:
  print ("Metashape Activation returned false, exiting")
  sys.exit(128)
projectPath =  path_photos + '/' + projectName + '-' + orbitName + '.psz'
""" We are always running this against a subdir """
jobSettingsFile = open(path_photos + '/../jobsettings.yaml')
jobSettings = yaml.load(jobSettingsFile)

""" Settings we need """
imageExtensions = jobSettings['image-extensions']

"""
  Build the list of images in this directory we only want jpg jpeg tif tiff and png for now
  append them to the chunklist and save the project
"""
runMask = False
chunk = doc.addChunk()
image_list = os.listdir(path_photos)
photo_list = list()
for photo in image_list:
  if "." not in photo or photo == "."  or photo == "..":
    continue
  if photo.rsplit(".",1)[1].lower() in imageExtensions:
    if photo.rsplit(".",1)[1].lower() == 'png':
        runMask = True
        maskPath = ''
    if photo.rsplit(".",1)[1].lower() == 'jpg':
      if os.path.exists(path_photos + '/masks'):
        runMask = True
        """ Check to see if there's a single mask, not 1 per file """
        if os.path.exists(path_photos + '/masks/mask.jpg'):
          maskPath = 'masks/mask.jpg'
        else:
          maskPath = 'masks/{filename}.jpg'
      else:
        runMask = False
    photo_list.append("/".join([path_photos, photo]))
chunk.addPhotos(photo_list)
""" Import Masks - This isn't always done so check job-settings to see if this orbit is a masked one """
if runMask:
  if maskPath == '':    
    chunk.importMasks(path=maskPath,source=Metashape.MaskSourceAlpha, operation=Metashape.MaskOperationReplacement, tolerance=20)
  else:
    chunk.importMasks(path=maskPath,source=Metashape.MaskSourceFile, operation=Metashape.MaskOperationReplacement, tolerance=20)
else:
  print("No PNGs found, skipping mask for this align")
chunk.matchPhotos(downscale=1, generic_preselection=True, reference_preselection=True,reference_preselection_mode=Metashape.ReferencePreselectionSequential, filter_mask=True,mask_tiepoints=True, tiepoint_limit=0, keypoint_limit=400000)
chunk.alignCameras()
realign_list = list()
print("Initial Alignment completed, attempting second alignment of unaligned cameras")
for camera in chunk.cameras:
  if not camera.transform:
    realign_list.append(camera)
""" Do a second pass at aligning cameras """
chunk.alignCameras(cameras = realign_list)

""" Now filter out junk """
print('Filtering points based on jobSettings::filter settings')
if 'ImageCount' in jobSettings['filter'].keys():
  cloudfilter = Metashape.PointCloud.Filter()
  cloudfilter.init(chunk,criterion = Metashape.PointCloud.Filter.ImageCount)
  cloudfilter.removePoints(int(jobSettings['filter']['ImageCount']))

if 'ReconstructionUnvertainty' in jobSettings['filter'].keys():
  cloudfilter = Metashape.PointCloud.Filter()
  cloudfilter.init(chunk, criterion= Metashape.PointCloud.Filter.ReconstructionUncertainty)
  cloudfilter.removePoints(int(jobSettings['filter']['ReconstructionUnvertainty']))

doc.save(path = projectPath)
print("Saved project:",projectPath,"exiting",sep=" ")
sys.exit(0)
