#!/usr/bin/env bash
## This script will submit a job for the metashape hotness, with the first argument as the directory 
## that should be proccessed
NODE_HOSTNAME=`hostname`
echo "Activating License on $NODE_HOSTNAME"
echo "Using license $METASHAPE_LICENSE"
echo "Add license file path [$HOME/metashape-pro/license/$NODE_HOSTNAME/] to environment, using subdir of ${NODE_HOSTNAME}"
export AGISOFT_REHOST_PATH=$HOME/metashape-pro/license/$NODE_HOSTNAME/
mkdir -p $HOME/metashape-pro/license/$NODE_HOSTNAME/
if [[ -z "${METASHAPE_LICENSE}" ]]; then
  echo "No Metashape node locked license, checking for floating license"
  if [[ -z "${agisoft_LICENSE}" ]]; then
  	echo "No floating license found, exiting"
	exit 128
  fi
else
  $HOME/metashape-pro/metashape --activate $METASHAPE_LICENSE
  if [ $? -eq 0 ]; then
   	echo "Metashape License Activation succeeded, continuing"
  else
	echo "License Failure"
	exit 128
  fi
fi
echo "Load Python/3.5.4 module"
module load StdEnv/2018.3
module load python/3.5.4
module load qt/5.13.1
module load openblas/0.3.4
module load mesa/18.3.6
echo "Add Python 3.5.4 lib to ld path"
export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/soft.computecanada.ca/easybuild/software/2017/Core/python/3.5.4/lib
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/soft.computecanada.ca/easybuild/software/2017/Core/qt/5.13.1/lib
#export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/cvmfs/soft.computecanada.ca/easybuild/software/2017/avx2/Compiler/intel2016.4/openblas/0.3.20/lib
echo "Running Metashape $2 on $1 for ${3}"
export METASHAPE_PROJECT_NAME=${3}
export METASHAPE_DATA_PATH=$1 
cd $METASHAPE_DATA_PATH
if [ $2 = 'align' ] 
then
  ${HOME}/metashape-pro/metashape.sh -r ${HOME}/project/${USER}/job-submission/metashape/metashape-align.py -platform offscreen
  if [ $? -ne 0 ]
  then
    echo "Metashape may have crashed, re-opening/closing to try to clear license"
    ${HOME}/metashape-pro/metashape.sh -r ${HOME}/project/${USER}/job-submission/metashape/metashape-reinit.py -platform offscreen
  fi
  echo "Schedule next orbit job"
  NEXTCOMMAND="${HOME}/project/${USER}/job-submission/metashape/run-metashape ${METASHAPE_PROJECT_NAME} align"
fi
if [ $2 = 'cloud' ]
then
  ${HOME}/metashape-pro/metashape.sh -r ${HOME}/project/${USER}/job-submission/metashape/metashape-cloud.py -platform offscreen
  if [ $? -ne 0 ]
  then
    echo "Metashape may have crashed, re-opening/closing to try to clear license"
    ${HOME}/metashape-pro/metashape.sh -r ${HOME}/project/${USER}/job-submission/metashape/metashape-reinit.py -platform offscreen
  fi
  echo "Dense Cloud Completed"
  echo "Schedule next cloud job"
  NEXTCOMMAND="${HOME}/project/${USER}/job-submission/metashape/run-metashape ${METASHAPE_PROJECT_NAME} cloud"
fi
if [ $2 = 'filter' ]
then
  ${HOME}/metashape-pro/metashape.sh -r ${HOME}/project/${USER}/job-submission/metashape/metashape-filter.py -platform offscreen
  if [ $? -ne 0 ]
  then
    echo "Metashape may have crashed, re-opening/closing to try to clear license"
    ${HOME}/metashape-pro/metashape.sh -r ${HOME}/project/${USER}/job-submission/metashape/metashape-reinit.py -platform offscreen
  fi
  echo "Filter completed"
  echo "Scheduling next filter job"
  NEXTCOMMAND="${HOME}/project/${USER}/job-submission/metashape/run-metashape ${METASHAPE_PROJECT_NAME} filter"
fi
if [ $2 = 'refine' ]
then
  ${HOME}/metashape-pro/metashape.sh -r ${HOME}/project/${USER}/job-submission/metashape/metashape-refine.py -platform offscreen
  if [ $? -ne 0 ]
  then
    echo "Metashape may have crashed, re-opening/closing to try to clear license"
    ${HOME}/metashape-pro/metashape.sh -r ${HOME}/project/${USER}/job-submission/metashape/metashape-reinit.py -platform offscreen
  fi
  echo "Refine Cloud Completed"
  echo "Schedule next refine job"
  NEXTCOMMAND="${HOME}/project/${USER}/job-submission/metashape/run-metashape ${METASHAPE_PROJECT_NAME} refine"
fi


# If using a floating license, we don't need to deactivate it
if [[ -z "${METASHAPE_LICENSE}" ]]; then
  echo "No Metashape node locked license, assuming floating license."
else
  echo "Deactivating license"
  until $HOME/metashape-pro/metashape --deactivate
  do
   sleep 10
   echo "Sleeping, and re-trying deactivation"
  done
  echo "License deactivated d(o.o)b"
fi
# Run the next orbit
if [ -z "${NEXTCOMMAND}" ]
then
  echo "No more jobs to schedule"
else
  echo "Excuting ${NEXTCOMMAND}"
  $NEXTCOMMAND
fi
echo " Job completed on $NODE_HOSTNAME, exiting cleanly"
exit 0
