### AGISOFT METASHAPE job submission scripts

This script is released under the GNUGPLv2 - Copyright Karl Vollmer <karl.vollmer@gmail.com> 

#### Requirements

 These scripts are written to only work with Metashape 1.6.x


#### Overview

To run a job on computecanada 

 * Copy jobsettings.yaml.dist to jobsettings.yaml and fill it out
   * You can put a jobsettings.yaml into the photo directory, if a specific run needs different settings
 * Open the submit_metashap.sh and set the project path at the top FIXME
 * Copy metashape.settings.dist to metashape.settings and set the path to the photos
 * If you are using a Node locked license run `export METASHAPE_LICENSE=[YOUR LICENSE KEY]` if you are
   using a floating license then run `export agisoft_LICENSE=/path/to/your/metashape.lic` before running
   a job, you can also do this inline to your run command, see below for an example of node locked license and run command at the same time
 * Run `METASHAPE_LICENSE=<LICENSE> ./run-metashape <PHOTO Subdir> <Operation> <Force>`

You can also just set METASHAPE_LICENSE for your entire session with an `export METASHAPE_LICENSE=` command

#### Utility scripts

 There are some included bash scripts to help make large processing a bit easier

#### run-all-metashape

 This script will itterate through `./photos` and run the specified process against all directories found. 

 Examples: 
  * `run-all-metashape align force` would force an align of all artifact orbits in ./photos
  * `run-all-metashape cloud` would run a dense cloud on all artifact orbits that have not already run in ./photos

#### rerun-all-metashape

  This script looks in the current directory for `*.out` files, and looks for known "errors" any jobs that have errored
  are re-run with a force. This may be updated reguarlly as we find new ways for metashape to fail

  This script takes no arguments

## File organization assumption

 There is an assumption that photos are in `path/[Run Name]/[Orbit Name]/{IMAGES}` a single run name
 can have many orbits, the script will itterate over all orbits within a Run name and run the requested operation. 

**Example: Given the following**
```
path/CoopersFerry/Pass1/photo000-200.jpg
path/CoopersFerry/Pass1/masks/mask.jpg
path/CoopersFerry/Pass2/photo000-200.jpg
path/CoopersFerry/Pass2/masks/photo000-200.jpg
```

If you ran `./run-metashape CoopersFerry align` the script would submit a job for Pass1, which would
see the masks subdir with a single file, and use that mask for all photos. The PSZ would be saved in the /Pass1 directory
once Pass1 finished, it would submit a second job for Pass2 before halting. Once the Pass2 job started it would see the masks dir with a file per photo
and use `masks/{filename}.jpg` for the masking

## Currently Supported Operations

#### Align

 `metashape-run CoopersFerry align`

 An align will do the following

 * If image type is "PNG" these can have integrated masks, do not look for a mask subdir
 * If photo type is "JPG" look for a ./masks subdir
   * If there is a ./masks/mask.jpg, use that mask for all photos
   * Otherwise use masks/{filename}.jpg as the mask
 * Add photos `chunk.addPhotos(photo_list)`
 * If Masks then run `chunk.importMasks(path=maskPath,source=Metashape.MaskSourceAlpha, operation=Metashape.MaskOperationReplacement, tolerance=20)`
 * Run `chunk.matchPhotos(accuracy=Metashape.HighestAccuracy, generic_preselection=False, reference_preselection=False,filter_mask=True,mask_tiepoints=False,tiepoint_limit=0,keypoint_limit=400000)`
 * Run `chunk.alignCameras()`
 * Itterate over all cameras, find all cameras that did not align properly, and try again, this has minimal performance impact, and seems to catch a few the first pass missed
 * Save Project file as a psz in the photo directory

#### filter

 `metashape-run CoppersFerry filter`

 Two filter operations are currently supported and are controlled by the jobSettings `filter` settings
 This can be run independently, but it is also part of the standard align process

 * ImageCount: Filter out points with less than the defined number of images, default is 2
 * ReconstructionUncertainty: Filter out points with less than the defined amount of certainty, default is 25

#### cloud

 `metashape-run CoopersFerry cloud`

 A dense cloud will do the following

 * Tell metashape to use up to 15 GPUS (aka all)
 * Load the psz file from the [Orbit Name]
 * run `chunk.buildDepthMaps(quality=Metashape.UltraQuality, filter=Metashape.MildFiltering,reuse_depth=False)`
 * run `chunk.buildDenseCloud(point_colors=True)`
 * Save project

#### refine

 `metashape-run CoopersFerry refine` 

 A refine will do the following

 * Tell metashape to use up to 15 GPUS (aka all)
 * Load the psz file from the [Orbit Name]
 * run chunk.refineModel(quality=Metashape.UltraQuality, iterations=20, smoothness=0.5)
 * Save the project
