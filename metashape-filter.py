#!/usr/bin/env python
# vim: set softtabstop=2 ts=2 sw=2 expandtab:
import os, Metashape, sys, yaml 

""" 
 Create the metashape document that we will be proccessing
 Data Dir: The directory comes from the environment variable METASHAPE_DATA_PATH
 Project Name: Data Dir + SLURM JOB ID + -project.psz
"""
projectName = os.environ['METASHAPE_PROJECT_NAME']
path_photos = os.environ['METASHAPE_DATA_PATH']
orbitName = os.path.basename(os.path.normpath(path_photos))
projectPath =  path_photos + '/' + projectName + '-' + orbitName + '.psz'
""" Set the Metashape settings """
Metashape.app.Settings.log_enable = True
Metashape.app.gpu_mask = 15
Metashape.app.Settings.log_path = path_photos+'/' + os.environ['SLURM_JOB_ID'] + '-metashape.log'
""" We are always running this against a subdir """
try:
  jobSettingsFile = open(path_photos + '/../jobsettings.yaml')
  jobSettings = yaml.load(jobSettingsFile)
except:
  print("Error:: Unable to open job settings file %s" % (path_photos + '/../jobsettings.yaml'))
  sys.exit(128)
"""
  Build the list of images in this directory we only want jpg jpeg tif tiff and png for now
  append them to the chunklist and save the project
"""
doc = Metashape.app.document
doc.open(projectPath)
chunk = doc.chunks[0]

""" 
  We need to filter out images that are unlikely to be correct 

"""
if 'ImageCount' in jobSettings['filter'].keys():
	cloudfilter = Metashape.PointCloud.Filter()
	cloudfilter.init(chunk,criterion = Metashape.PointCloud.Filter.ImageCount)
	cloudfilter.removePoints(int(jobSettings['filter']['ImageCount']))

if 'ReconstructionUncertainty' in jobSettings['filter'].keys():
	cloudfilter = Metashape.PointCloud.Filter()
	cloudfilter.init(chunk, criterion= Metashape.PointCloud.Filter.ReconstructionUncertainty)
	cloudfilter.removePoints(int(jobSettings['filter']['ReconstructionUncertainty']))

""" Either way we optimize the cameras when we are done """
""" FIXME: This should be controlable as to the settings used """
chunk.optimizeCameras(fit_f=True, fit_cx=True, fit_cy=True, fit_b1=True, fit_b2=True, fit_k1=True, fit_k2=True, fit_k3=True, fit_k4=True, fit_p1=True, fit_p2=True, fit_corrections=True, adaptive_fitting=False, tiepoint_covariance=False)

doc.save(path= projectPath)
print("Filtering complete - saved project: %s" % (projectName))
sys.exit(0)
